package com.fercugliandro.mastertech.exercicio.emitirnfelogconsumer.service.impl;

import com.fercugliandro.mastertech.exercicio.emitirnf.model.RegistrarLog;
import com.fercugliandro.mastertech.exercicio.emitirnfelogconsumer.service.EmitirNFeLogService;
import org.springframework.stereotype.Service;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

@Service
public class EmitirNFeLogServiceImpl implements EmitirNFeLogService {

    @Override
    public void registrarLog(RegistrarLog log) {

        String arquivo = "/development/mastertech/emissao-nfe-log.txt";

        try {
            String registro = "";
            if ("emissao".equalsIgnoreCase(log.getTipo())) {
                registro = String.format("[%s] [%s]: [%s] acaba de pedir a emmissão de uma NF no valor de %s !\n", log.getTimestamp(), log.getTipo(), log.getIdentidade(), log.getValor());
            } else {
                registro = String.format("[%s] [%s]: [%s] acaba acaba de pedir os dados das suas notas fiscais. !\n", log.getTimestamp(), log.getTipo(), log.getIdentidade());
            }
            Files.write(Paths.get(arquivo), registro.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

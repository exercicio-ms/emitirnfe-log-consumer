package com.fercugliandro.mastertech.exercicio.emitirnfelogconsumer.service;

import com.fercugliandro.mastertech.exercicio.emitirnf.model.RegistrarLog;

public interface EmitirNFeLogService {

    void registrarLog(RegistrarLog log);

}

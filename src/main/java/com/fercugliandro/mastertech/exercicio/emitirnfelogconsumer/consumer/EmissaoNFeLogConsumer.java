package com.fercugliandro.mastertech.exercicio.emitirnfelogconsumer.consumer;

import com.fercugliandro.mastertech.exercicio.emitirnf.model.RegistrarLog;
import com.fercugliandro.mastertech.exercicio.emitirnfelogconsumer.service.EmitirNFeLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class EmissaoNFeLogConsumer {

    @Autowired
    private EmitirNFeLogService logService;

    @KafkaListener(topics = "luis-biro-1", groupId = "cugli-1")
    public void receberNFe(@Payload RegistrarLog log) {
        logService.registrarLog(log);
    }
}
